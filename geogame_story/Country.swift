//
//  Country.swift
//  geogame_story
//
//  Created by Kacper Ludwiński on 30/9/20.
//

import Foundation

struct Country : Decodable {
    let name: String
    let capital: String
}

class GenerateCountry {
    
    func load_country_info (number: Int, completion: @escaping (Country) -> ()) {
        
        let url = "https://www.restcountries.eu/rest/v2/all"
        let request = URLRequest(url: URL(string: url)!)
        var some_country: Country = Country(name: "", capital: "")


                       URLSession.shared.dataTask(with: request) { (data, response, error) in
                        
                           do {
                                let countries = try JSONDecoder().decode([Country].self, from: data!)
                                var i = 0
                                for country in countries {
                                    if i == number {
                                        some_country = Country(name: country.name, capital: country.capital)
                                    }
                                    i += 1
                                }
                            
                           } catch {
                               print("CATCH ERROR")
                           }
                        completion(some_country)
                        
        }.resume()
        
    }
}
