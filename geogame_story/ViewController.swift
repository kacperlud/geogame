//
//  ViewController.swift
//  geogame_story
//
//  Created by Kacper Ludwiński on 30/9/20.
//

import UIKit

class ViewController: UIViewController {
    

    @IBOutlet weak var name_country: UILabel!
    @IBOutlet weak var capital_name: UITextField!
    @IBOutlet weak var current_points: UILabel!
    var capital_name_true = ""
    var capital_name_enter = ""
    var score = 0
    var counter = 0
    var number = 0
    let generate_country = GenerateCountry()
    
    
    
    @IBAction func button(_ sender: UIButton) {
        capital_name_enter = capital_name.text!
        points(capital_enter: capital_name_enter, true_capital: capital_name_true)
        self.capital_name.text! = ""
        new_country()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if counter == 0 {
            new_country()
            counter += 1
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(didGetNotification(_:)), name: Notification.Name("text"), object: nil)
    }
    
    func new_country(){
        
        number = Int.random(in: 0...249)
        generate_country.load_country_info(number: number) { (results: Country) in
            self.capital_name_true = results.capital
            print(results)
            
            NotificationCenter.default.post(name: Notification.Name("text"), object: results.name)

        }
        
    }
    
    @objc func didGetNotification(_ notification: Notification) {
        let text = notification.object as! String?
        
        DispatchQueue.main.async {
            self.name_country.text = text
        }
    }
    
    func points(capital_enter: String, true_capital: String) {

        if capital_enter == true_capital {

            print("good")
            score += 1
            self.current_points.text = String(score)
        }
        else {
            print("bad")
        }
    }
}

